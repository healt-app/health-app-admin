import React, { useEffect } from 'react';
import '../App.css';
import initialImage from '../assets/initial-image.jpg'
// api
import { imageUrl } from '../components/Api';

function Previewer(props) {

    function overideDoom() {
        let element = window.document.getElementById('inner-html').getElementsByTagName('img');
        function doRide() {
            setTimeout(
                function () {
                    for (let data of element) {
                        data.classList.add('image-content');
                    }
                }, 1000);
        }
        doRide()
    }

    useEffect(() => {
        overideDoom()
    }, [])

    return (
        <div className='android-frame'>
            <div className='device-container'>
                <img
                    src={props.mainData.imageTitle.length === 0 ? initialImage : imageUrl + props.mainData.imageTitle}
                    alt={props.mainData.imageTitle}
                    className='image-title'
                />
                <div
                    style={{padding: '5px'}}
                    id='inner-html'
                    dangerouslySetInnerHTML={props.markup}
                />
            </div>
        </div>
    );
}

export default Previewer;
