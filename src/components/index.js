import React, { useEffect } from 'react';
import logo from '../assets/logo.svg';
import '../App.css';
import { Link } from 'react-router-dom';
import { from } from 'rxjs';

function Welcome() {

    useEffect(() => {
        console.log('this app is middleware for healtApp')
    }, [])


    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <p>
                    <b>Welcome to middleware healtApp application </b><br />
                    This app uses backend when created using &nbsp;
                    <a
                        className="App-link"
                        href="https://healt-app-api.herokuapp.com"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        nodeJS & MYSQL database
                    </a><br />
                    Open Menu Management HealtApp &nbsp;
                    <Link className="App-link" to="/menu">Here</Link>                    
                </p>
            </header>
        </div>
    );
}

export default Welcome;
