import React, { useEffect, useState } from 'react';
import '../App.css';
import { Typography, Grid, TextField, Button, Dialog, DialogTitle, DialogContent } from '@material-ui/core';
import { Image, AddBox } from '@material-ui/icons';
import { Api, imageUrl } from '../components/Api';


function ImageStore(props) {

    const [imageStore, setImageStore] = useState([])

    useEffect(() => {
        getImage()
    }, [])

    function getImage() {
        Api.get('/list-image')
            .then(res => {
                if (res.data.status === 200) {
                    let data = res.data.payload;
                    setImageStore(res.data.payload);
                } else {
                    alert('error')
                }
            })
            .catch(err => {
                alert(err)
            })
    }

    function handleUploadFile(event) {
        const data = new FormData();
        data.append('file', event.target.files[0]);
        Api.post('/fileUploader', data)
            .then(res => {
                getImage()
            })
            .catch(err => {
                alert(err)
            })
    }

    return (
        <Dialog onClose={props.handleClose} open={props.open}>
            <DialogTitle onClose={props.handleClose} className='secondary-bg'>
                <Typography
                    style={{
                        color: '#FFF',
                        fontSize: '25px'
                    }}>
                    Choose Image From Database
                    </Typography>
            </DialogTitle>
            <DialogContent dividers>
                <div>
                    <Grid container spacing={3}>
                        {imageStore.map((data, i) => (
                            <Grid item xs={4} key={i.toString()}>
                                <Button
                                    onClick={() => { props.chooseImage(data) }}
                                >
                                    <img
                                        src={imageUrl + data.image}
                                        alt={data.original_name}
                                        className='image-store-preview'
                                    />
                                </Button>
                                <p className='title-menu'>
                                    {data.image}
                                </p>
                            </Grid>
                        ))}
                        <Grid item xs={4} className='flex-center-column' style={{
                            justifyContent: 'space-around'
                        }}>
                            <input
                                accept="image/*"
                                style={{
                                    display: 'none'
                                }}
                                id={'upload-id'}
                                type="file"
                                onChange={handleUploadFile}
                            />
                            <label htmlFor={'upload-id'}>
                                <Button
                                    style={{ padding: 0 }}
                                    component="span"
                                >
                                    <AddBox color={'secondary'} style={{
                                        fontSize: '130px',
                                        textAlign: 'center'
                                    }} />
                                </Button>
                            </label>
                            <p className='title-menu'>
                                Add Image
                                </p>
                        </Grid>
                    </Grid>
                </div>
            </DialogContent>
        </Dialog>
    );
}

export default ImageStore;
