import axios from 'axios';

// Address
// const Host = 'http://192.168.17.1:4000';
// const Host = 'http://192.168.43.49:4000';
// const Host = 'http://10.10.118.119:4000';
// const Host = 'http://10.10.118.163:4000';
const Host = 'https://healt-app-api.herokuapp.com';

// Main URL
const appServer = Host;

// Interface
const Menu = 'menu';

// const isIE = /*@cc_on!@*/false || !!document.documentMode;

const header = {
  'Content-Type': 'application/json'
}

export const imageUrl = appServer + '/ftp/uploads/';

export const Api =
  axios.create({
    baseURL: appServer,
    header,
    timeout: 30000
  })