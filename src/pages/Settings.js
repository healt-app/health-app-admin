import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { Typography, Grid, TextField, Button, Dialog, DialogTitle, DialogContent } from '@material-ui/core';
import { Image, AddBox } from '@material-ui/icons';
import { EditorState, convertToRaw, convertFromRaw, ContentState, convertFromHTML } from 'draft-js';
// assets
import initialImage from '../assets/initial-image.jpg'
// api
import { Api, imageUrl } from '../components/Api';

function Settings(props) {

    const [modal, setModal] = useState({
        open: false,
        flag: true
    });
    const [mainData, setMainData] = useState({
        bannerImage: '',
    })
    const [imageStore, setImageStore] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        getPayload();
        getImage();
    }, []);

    function getPayload() {
        Api.get('/setting')
            .then(res => {
                if (res.data.status === 200) {
                    const data = res.data.payload;
                    setMainData({
                        ...mainData,                        
                        ['bannerImage']: data.banner_image
                    });
                } else {
                    alert(res)
                }
            })
            .catch(err => {
                console.log(err)
                alert(err)
            })
    }

    function editMenu() {
        setLoading(true)
        let payload = {            
            bannerImage: mainData.bannerImage,
        }

        Api.put('/setting', payload)
            .then(res => {
                setLoading(false)
                if (res.data.status === 200) {
                    alert('data berhasil diEdit')
                    props.history.goBack()
                } else {
                    alert(res)
                }
            })
            .catch(err => {
                alert(err)
            })

    }

    function handleUploadFile(event) {
        const data = new FormData();
        data.append('file', event.target.files[0]);
        Api.post('/fileUploader', data)
            .then(res => {
                getImage()
            })
            .catch(err => {
                alert(err)
            })
    }

    function getImage() {
        Api.get('/list-image')
            .then(res => {
                if (res.data.status === 200) {
                    let data = res.data.payload;
                    setImageStore(res.data.payload);
                } else {
                    alert('error')
                }
            })
            .catch(err => {
                alert(err)
            })
    }

    function chooseImage(data) {
        setModal({ ...modal, ['open']: false });
        setMainData({ ...mainData, ['bannerImage']: data.image });
    }

    function ModalImageStore() {
        return (
            <Dialog onClose={handleClose} open={modal.open}>
                <DialogTitle onClose={handleClose} className='secondary-bg'>
                    <Typography
                        style={{
                            color: '#FFF',
                            fontSize: '25px'
                        }}>
                        Choose Image From Database
                    </Typography>
                </DialogTitle>
                <DialogContent dividers>
                    <div>
                        <Grid container spacing={3}>
                            {imageStore.map((data, i) => (
                                <Grid item xs={4} key={i.toString()}>
                                    <Button
                                        onClick={() => { chooseImage(data) }}
                                    >
                                        <img
                                            src={imageUrl + data.image}
                                            alt={data.original_name}
                                            className='image-store-preview'
                                        />
                                    </Button>
                                    <p className='title-menu'>
                                        {data.image}
                                    </p>
                                </Grid>
                            ))}
                            <Grid item xs={4} className='flex-center-column' style={{
                                justifyContent: 'space-around'
                            }}>
                                <input
                                    accept="image/*"
                                    style={{
                                        display: 'none'
                                    }}
                                    id={'upload-id'}
                                    type="file"
                                    onChange={handleUploadFile}
                                />
                                <label htmlFor={'upload-id'}>
                                    <Button
                                        style={{ padding: 0 }}
                                        component="span"
                                    >
                                        <AddBox color={'secondary'} style={{
                                            fontSize: '130px',
                                            textAlign: 'center'
                                        }} />
                                    </Button>
                                </label>
                                <p className='title-menu'>
                                    Add Image
                                </p>
                            </Grid>
                        </Grid>
                    </div>
                </DialogContent>
            </Dialog>
        )
    }

    const handleClickOpen = (flag) => {
        if (flag === false) {
            console.log('sip')
        }
        setModal({ ...modal, ['open']: true });
    };
    const handleClose = () => {
        setModal({ ...modal, ['open']: false });
    };
 
    return (
        <div>
            <ModalImageStore />
            <div className='flex-center-column'>
                <div style={{
                    maxWidth: '1000px'
                }}>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <Grid item style={{ padding: '10px' }}>
                                <div className={'container-editor-title'}>
                                    <div className='flex-center-column primary-bg' style={{
                                        borderRadius: '15px 15px 0 0'
                                    }}>
                                        <h1 style={{ margin: '5px' }}>Settings</h1>
                                    </div>
                                    <div style={{
                                        padding: '10px'
                                    }}>
                                        <Grid container>
                                            <Grid item xs={8} className='flex-center-column'>
                                                <div style={{ width: '350px' }}>
                                                    <Button
                                                        variant="contained"
                                                        component="span"
                                                        size={'large'}
                                                        color={'secondary'}
                                                        fullWidth
                                                        onClick={() => {
                                                            handleClickOpen()
                                                        }}
                                                    >
                                                        <Image /> &nbsp;
                                                        Ganti Foto Banner
                                                    </Button>
                                                </div>
                                            </Grid>
                                            <Grid item xs={4} style={{ padding: '10px' }}>
                                                <div style={{
                                                    borderRadius: '17px',
                                                    border: '2px solid #2196F3'
                                                }}>
                                                    <div className='flex-center-column primary-bg' style={{
                                                        borderRadius: '15px 15px 0 0'
                                                    }}>
                                                        <h2 style={{ margin: '5px' }}>
                                                            Banner Preview
                                                </h2>
                                                    </div>
                                                    <div className='flex-center-column'
                                                        style={{ minHeight: '200px' }}>
                                                        <div className='flex-center-column'
                                                            style={{
                                                                minHeight: '200px'
                                                            }}>
                                                            <img
                                                                src={
                                                                    mainData.bannerImage.length === 0 ? initialImage : imageUrl + mainData.bannerImage
                                                                }
                                                                alt='image'
                                                                className='image-profile-preview'
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </Grid>
                                        </Grid>
                                    </div>
                                </div>
                            </Grid>                           
                            <Grid item>
                                <Button
                                    variant='contained'
                                    size='large'
                                    fullWidth
                                    disabled={loading}
                                    color='secondary'
                                    size='large'
                                    onClick={() => {
                                        editMenu()
                                    }}
                                >
                                    Save Settings
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </div>
            </div>
        </div>
    );
}

export default withRouter(Settings);