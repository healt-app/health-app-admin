import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { Grid, Card, CardActionArea, CardContent, CardMedia, Typography } from '@material-ui/core';
import { AddBox } from '@material-ui/icons';
//Api
import { Api, imageUrl } from '../components/Api';
import Slide from '@material-ui/core/Slide';

function ListVideo(props) {

    const [list, setList] = useState([]);

    useEffect(() => {
        getVideo();
    }, []);

    function getVideo() {
        Api.get('/video')
            .then(res => {
                if (res.data.status === 200) {
                    setList(res.data.payload)
                } else {
                    alert(res)
                }
            })
            .catch(err => {
                alert(err)
            })
    }

    return (
        <Grid container spacing={3}>
            {list.map((row, index) => (
                <Grid item md={4} sm={6} xs={12} lg={3} key={index.toString()}>
                    <Card>
                        <CardActionArea onClick={() => {
                             props.history.push({
                                pathname: '/menu/video-management',
                                state: { payload: row }
                              })
                        }}>
                            <CardMedia
                                style={{ boxShadow: 'unset' }}
                                className='image-title'
                                image={imageUrl + row.video_image}
                                title={row.video_image}
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2" color='primary'>
                                    {row.title}
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </Grid>
            ))}
            <Grid item md={4} sm={6} xs={12} lg={3}>
                <Card>
                    <CardActionArea onClick={() => {
                        props.history.push('/menu/video-management')
                    }}>
                        <div
                            style={{
                                width: '100%',
                                height: '250px'
                            }}
                            className='flex-center-column'
                        >
                            <AddBox
                                color={'secondary'}
                                style={{
                                    fontSize: '200px'
                                }} />
                        </div>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2" color='primary'>
                                Add Video
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
            </Grid>
        </Grid >
    );
}

export default withRouter(ListVideo)
