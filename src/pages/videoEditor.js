import React, { useEffect, useState } from 'react';
import '../App.css';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { Typography, Button, Card, CardContent, CardMedia, Grid } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import ImageStore from '../components/ImageStore';
import InitialImage from '../assets/initial-image.jpg';
import { Api, imageUrl } from '../components/Api';
import { PlayCircleFilled } from '@material-ui/icons';
import { nullChecker } from '../components/PayloadChecker';

function VideoEditor(props) {

    const initialState = {
        title: '',
        description: '',
        videoImage: '',
        videoLink: ''
    }

    const [data, setData] = useState(initialState);
    const [open, setOpen] = useState(false);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if(props.history.location.state !== undefined){
            const payload = props.history.location.state.payload;
            setData({
                idVideo: payload.id,
                title: payload.title,
                description: payload.description,
                videoImage: payload.video_image,
                videoLink: payload.video_link
            })
        }
    }, []);

    const handleChange = name => event => {
        setData({ ...data, [name]: event.target.value });
    };

    function handleClose() {
        setOpen(false)
    }

    function closeEditor(){
        props.history.goBack()
    }

    function addVideo() {
        setLoading(true)        

        Api.post('/video', data)
            .then(res => {
                setLoading(false)
                if (res.data.status === 200) {
                    alert('data berhasil ditambah')
                    props.history.goBack();                                                      
                } else {
                    alert(res)
                }
            })
            .catch(err => {
                alert(err)
            })
    }

    function editVideo() {
        setLoading(true)        
        Api.put('/video', data)
            .then(res => {
                setLoading(false)
                if (res.data.status === 200) {
                    alert('data berhasil diubah')     
                    props.history.goBack();                                  
                } else {
                    alert(res);
                }
            })
            .catch(err => {
                alert(err)
            })
    }

    function deleteVideo() {
        setLoading(true)
        Api.delete('/video?idVideo=' + data.idVideo)
            .then(res => {
                setLoading(false)
                if (res.data.status === 200) {
                    alert('data berhasil di hapus')
                    props.history.goBack()
                } else {
                    alert(res)
                }
            })
            .catch(err => {
                alert(err)
            })

    }

    function chooseImage(payload) {
        setOpen(false)
        setData({ ...data, videoImage: payload.image })
    }

    function youtubeParser(url) {
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);
        var youtubeID = (match && match[7].length == 11) ? match[7] : false;
        if(youtubeID === false){
            alert('mohon jangan diketik >_< copas aja gan !!!')
        } else {
            setData({ ...data, videoLink: youtubeID })
        }
    }

    function stringFlag(){
        if(props.history.location.state === undefined){
            return 'Add Video';
        } else {
            return 'Edit Video';
        }
    }

    return (
            <div>
                <AppBar>
                    <Toolbar>
                        <IconButton edge="start" color="inherit"
                            onClick={()=> {
                                closeEditor()
                            }}>
                            <CloseIcon />                            
                        </IconButton>
                        <Typography variant="h6">
                            {stringFlag()}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div>
                    <div className='flex-center-all'>
                        <div id={'card-video'} style={{ width: '500px', maxHeight: 200 }}>
                            <Card>
                                <Grid container>
                                    <Grid item xs={6}>
                                        <div className='flex-center-column'>
                                            <CardContent style={{ textAlign: 'center' }}>
                                                <Typography component="h5" variant="h5">
                                                    {data.title}
                                                </Typography>
                                                <Typography variant="subtitle1" color="textSecondary">
                                                    {data.description}
                                                </Typography>
                                            </CardContent>
                                            <IconButton aria-label="play/pause">
                                                <PlayCircleFilled
                                                    color='secondary'
                                                    style={{ fontSize: '40px' }} />
                                            </IconButton>
                                        </div>
                                    </Grid>
                                    <Grid item xs={6} style={{maxHeight: 200}}>
                                        <img
                                            className='full'
                                            src={
                                                data.videoImage.length <= 2 ?
                                                    InitialImage :
                                                    imageUrl + data.videoImage}
                                            alt='123'
                                            style={{objectFit: 'cover'}}
                                        />
                                    </Grid>
                                </Grid>
                            </Card>
                        </div>
                        <div>
                            <iframe
                                width="500"
                                height="200"
                                src={'https://www.youtube.com/embed/' + data.videoLink}
                                frameborder="0"
                                allow="
                                accelerometer; 
                                autoplay; 
                                encrypted-media; 
                                gyroscope; 
                                picture-in-picture"
                                allowfullscreen
                            >
                            </iframe>
                        </div>
                    </div>
                    <div className='full flex-center-column'>
                        <div style={{ width: '500px' }}>
                            <TextField
                                label="Judul Video"
                                value={data.title}
                                onChange={handleChange('title')}
                                fullWidth
                                margin="normal"
                                variant="outlined"
                                inputProps={{
                                    maxLength: 15,
                                }}
                            />
                            <TextField
                                label="Diskripsi Video"
                                value={data.description}
                                onChange={handleChange('description')}
                                fullWidth
                                margin="normal"
                                variant="outlined"
                                inputProps={{
                                    maxLength: 50,
                                }}
                            />
                            <TextField
                                label="Youtube Link"
                                value={data.videoLink}
                                onChange={(event) => {
                                    youtubeParser(event.target.value)
                                }}
                                fullWidth
                                margin="normal"
                                variant="outlined"
                            />
                            <div className='flex-center-all'>
                                <Button
                                    onClick={() => {
                                        setOpen(true)
                                    }}
                                    fullWidth
                                    variant={'contained'}
                                    color='secondary'
                                    size='large'
                                >
                                    Pilih Gambar
                                </Button>
                                <Button
                                    fullWidth
                                    style={{
                                        marginLeft: 20
                                    }}
                                    size={'large'}
                                    color={'primary'}
                                    variant={'contained'}
                                    disabled={loading}
                                    onClick={() => {
                                        if(nullChecker(data)){
                                            alert('isi semua data dulu')
                                        } else {
                                            if(stringFlag() === 'Add Video'){
                                                addVideo()
                                            } else {
                                                editVideo()
                                            }
                                        }
                                    }}
                                >
                                    {stringFlag()}
                            </Button>
                            </div>
                            {stringFlag() === 'Add Video' ? (
                                <div />
                            ) : (
                                <div style={{paddingTop: '20px'}}>
                                <Button
                                    onClick={() => {
                                        var r = window.confirm("Are you Sure ?");
                                        if (r == true) {
                                            deleteVideo()                                            
                                        } else {
                                            console.log('cencel')
                                        }
                                    }}
                                    disabled={loading}
                                    fullWidth
                                    variant={'contained'}
                                    color='inherit'
                                >
                                    Hapus Video
                                </Button>
                            </div>
                            )}                            
                        </div>
                    </div>
                </div>
                <ImageStore
                    handleClose={handleClose}
                    open={open}
                    chooseImage={chooseImage}
                />
            </div>            
    );
}

export default VideoEditor;
