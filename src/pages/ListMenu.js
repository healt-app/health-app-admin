import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { Grid, Card, CardActionArea, CardActions, CardContent, CardMedia, Typography } from '@material-ui/core';
import { AddBox } from '@material-ui/icons';
//Api
import { Api, imageUrl } from '../components/Api';


function ListMenu(props) {

  const [list, setList] = useState([]);

  useEffect(() => {
    getMenu();
  }, []);

  function getMenu() {
    Api.get('/menu')
      .then(res => {
        if (res.data.status === 200) {
          setList(res.data.payload)
        } else {
          alert(res)
        }
      })
      .catch(err => {
        alert(err)
      })
  }

  return (
    <Grid container spacing={3}>
      {list.map((row, index) => (
        <Grid item md={4} sm={6} xs={12} lg={3} key={index.toString()}>
          <Card>
            <CardActionArea onClick={() => {
              props.history.push({
                pathname: 'menu/edit',
                state: { payload: row }
              })
            }}>
              <CardMedia
                style={{ boxShadow: 'unset' }}
                className='image-title'
                image={imageUrl + row.image_menu}
                title={row.image_menu}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2" color='primary'>
                  {row.title}
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      ))}
      <Grid item md={4} sm={6} xs={12} lg={3}>
        <Card>
          <CardActionArea onClick={() => {
            props.history.push('menu/add')
          }}>
            <div
              style={{
                width: '100%',
                height: '250px'
              }}
              className='flex-center-column'
            >
              <AddBox
                color={'secondary'}
                style={{
                  fontSize: '200px'
                }} />
            </div>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2" color='primary'>
                Add Menu
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
    </Grid >
  );
}

export default withRouter(ListMenu)
