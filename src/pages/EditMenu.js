import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import { Typography, Grid, TextField, Button, Dialog, DialogTitle, DialogContent } from '@material-ui/core';
import { Image, AddBox } from '@material-ui/icons';
import { EditorState, convertToRaw, convertFromRaw, ContentState, convertFromHTML } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
// import htmlToDraft from 'html-to-draftjs';
// assets
import initialImage from '../assets/initial-image.jpg'
// import androidFrame from '../assets/android-frame.png';
// api
import { Api, imageUrl } from '../components/Api';
import { nullChecker } from '../components/PayloadChecker';
import Previewer from '../components/previewer';

function EditMenu(props) {

    const [modal, setModal] = useState({
        open: false,
        flag: true
    });
    const [mainData, setMainData] = useState({
        ID: null,
        title: '',
        imageTitle: '',
    })
    // const [editorState, setEditorState] = useState(EditorState.createWithContent(state))
    const [editorState, setEditorState] = useState(EditorState.createEmpty())
    const [imageStore, setImageStore] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        getPayload();
        getImage();
    }, []);

    function initialMarkup(payload) {
        let innerHtml = JSON.parse(payload).__html;
        let blocksFromHTML = convertFromHTML(innerHtml);
        let state = ContentState.createFromBlockArray(
            blocksFromHTML.contentBlocks,
            blocksFromHTML.entityMap
        );
        setEditorState(EditorState.createWithContent(state))
    }

    function getPayload() {
        const data = props.location.state.payload;
        setMainData({
            ...mainData,
            ['ID']: data.id_menu,
            ['title']: data.title,
            ['imageTitle']: data.image_menu
        });
        initialMarkup(data.content_menu)
    }

    function editMenu() {
        setLoading(true)
        let payload = {
            idMenu: mainData.ID,
            title: mainData.title,
            imageMenu: mainData.imageTitle,
            contentMenu: JSON.stringify(createMarkup())
        }

        Api.put('/menu', payload)
            .then(res => {
                setLoading(false)
                if (res.data.status === 200) {
                    alert('data berhasil diEdit')
                    props.history.goBack()
                } else {
                    alert(res)
                }
            })
            .catch(err => {
                alert(err)
            })

    }

    function deleteMenu() {
        setLoading(true)
        Api.delete('/menu?idMenu=' + mainData.ID)
            .then(res => {
                setLoading(false)
                if (res.data.status === 200) {
                    alert('data berhasil di hapus')
                    props.history.goBack()
                } else {
                    alert(res)
                }
            })
            .catch(err => {
                alert(err)
            })

    }

    function handleUploadFile(event) {
        const data = new FormData();
        data.append('file', event.target.files[0]);
        Api.post('/fileUploader', data)
            .then(res => {
                getImage()
            })
            .catch(err => {
                alert(err)
            })
    }

    function getImage() {
        Api.get('/list-image')
            .then(res => {
                if (res.data.status === 200) {
                    let data = res.data.payload;
                    setImageStore(res.data.payload);
                } else {
                    alert('error')
                }
            })
            .catch(err => {
                alert(err)
            })
    }

    function upload(file) {
        const data = new FormData();
        data.append('file', file);
        return new Promise(
            (resolve, reject) => {
                Api.post('/fileUploader', data)
                    .then(res => {
                        resolve({
                            data: {
                                link: imageUrl + res.data.payload.filename
                            }
                        });
                    })
                    .catch(err => {
                        alert(err)
                    })
            }
        );
    }

    function chooseImage(data) {
        setModal({ ...modal, ['open']: false });
        setMainData({ ...mainData, ['imageTitle']: data.image });
    }

    function ModalImageStore() {
        return (
            <Dialog onClose={handleClose} open={modal.open}>
                <DialogTitle onClose={handleClose} className='secondary-bg'>
                    <Typography
                        style={{
                            color: '#FFF',
                            fontSize: '25px'
                        }}>
                        Choose Image From Database
                    </Typography>
                </DialogTitle>
                <DialogContent dividers>
                    <div>
                        <Grid container spacing={3}>
                            {imageStore.map((data, i) => (
                                <Grid item xs={4} key={i.toString()}>
                                    <Button
                                        onClick={() => { chooseImage(data) }}
                                    >
                                        <img
                                            src={imageUrl + data.image}
                                            alt={data.original_name}
                                            className='image-store-preview'
                                        />
                                    </Button>
                                    <p className='title-menu'>
                                        {data.image}
                                    </p>
                                </Grid>
                            ))}
                            <Grid item xs={4} className='flex-center-column' style={{
                                justifyContent: 'space-around'
                            }}>
                                <input
                                    accept="image/*"
                                    style={{
                                        display: 'none'
                                    }}
                                    id={'upload-id'}
                                    type="file"
                                    onChange={handleUploadFile}
                                />
                                <label htmlFor={'upload-id'}>
                                    <Button
                                        style={{ padding: 0 }}
                                        component="span"
                                    >
                                        <AddBox color={'secondary'} style={{
                                            fontSize: '130px',
                                            textAlign: 'center'
                                        }} />
                                    </Button>
                                </label>
                                <p className='title-menu'>
                                    Add Image
                                </p>
                            </Grid>
                        </Grid>
                    </div>
                </DialogContent>
            </Dialog>
        )
    }

    const handleClickOpen = (flag) => {
        if (flag === false) {
            console.log('sip')
        }
        setModal({ ...modal, ['open']: true });
    };
    const handleClose = () => {
        setModal({ ...modal, ['open']: false });
    };

    const handleChange = name => event => {
        setMainData({ ...mainData, [name]: event.target.value });
    };

    function onEditorStateChange(editorState) {
        setEditorState(editorState)
    };

    function createMarkup() {
        return { __html: draftToHtml(convertToRaw(editorState.getCurrentContent())) };
    }

    return (
        <div>
            <ModalImageStore />
            <Grid container spacing={3}>
                <Grid item xs={12} md={7}>
                    <Grid item style={{ padding: '10px' }}>
                        <div className={'container-editor-title'}>
                            <div className='flex-center-column primary-bg' style={{
                                borderRadius: '15px 15px 0 0'
                            }}>
                                <h1 style={{ margin: '5px' }}>Menu Editor</h1>
                            </div>
                            <div style={{
                                padding: '10px'
                            }}>
                                <Grid container>
                                    <Grid item xs={8} className='flex-center-column'>
                                        <div style={{ width: '350px' }}>
                                            <TextField
                                                label="Title"
                                                value={mainData.title}
                                                onChange={handleChange('title')}
                                                margin="normal"
                                                placeholder={'Input Title'}
                                                variant="outlined"
                                                fullWidth
                                                inputProps={{
                                                    maxLength: 20,
                                                }}
                                            />
                                            <div style={{ height: '30px' }} />
                                            <Button
                                                variant="contained"
                                                component="span"
                                                size={'large'}
                                                color={'secondary'}
                                                onClick={() => {
                                                    handleClickOpen()
                                                }}
                                            >
                                                <Image /> &nbsp; Upload Image
                                            </Button>
                                        </div>
                                    </Grid>
                                    <Grid item xs={4} style={{ padding: '10px' }}>
                                        <div style={{
                                            borderRadius: '17px',
                                            border: '2px solid #2196F3'
                                        }}>
                                            <div className='flex-center-column primary-bg' style={{
                                                borderRadius: '15px 15px 0 0'
                                            }}>
                                                <h2 style={{ margin: '5px' }}>
                                                    Menu Preview
                                                </h2>
                                            </div>
                                            <div className='flex-center-column' style={{ minHeight: '200px' }}>
                                                <div className='menu-preview'>
                                                    <div className='flex-center-column' style={{
                                                        height: 'calc(100% - 30px)'
                                                    }}>
                                                        <img
                                                            src={mainData.imageTitle.length === 0 ? initialImage : imageUrl + mainData.imageTitle}
                                                            alt='image'
                                                            className='image-menu'
                                                        />
                                                    </div>
                                                    <div>
                                                        <p className='title-menu'>{mainData.title}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </Grid>
                                </Grid>
                            </div>
                        </div>
                    </Grid>
                    <Grid item>
                        <div className={'container-editor'}>
                            <Editor
                                editorState={editorState}
                                wrapperClassName="wrapper"
                                editorClassName="editor"
                                onEditorStateChange={onEditorStateChange}
                                toolbar={{
                                    image: {
                                        // uploadCallback: upload,
                                        previewImage: true,
                                        uploadEnabled: true
                                    }
                                }}
                            />
                        </div>
                    </Grid>
                </Grid>
                <Grid item xs={12} md={5}>
                    <div className='container-editor-title' style={{
                        marginTop: '10px'
                    }}>
                        <div className='flex-center-column primary-bg'
                            style={{
                                borderRadius: '15px 15px 0 0'
                            }}>
                            <h1 style={{ margin: '5px' }}>Content Preview</h1>
                        </div>
                        <div className='flex-center-column'
                            style={{
                                padding: '50px 0'
                            }}>
                            <Previewer markup={createMarkup()} mainData={mainData} />
                        </div>
                    </div>
                    <div className='space' />
                    <Button
                        variant='contained'
                        size='large'
                        fullWidth
                        disabled={loading}
                        color='secondary'
                        onClick={() => {
                            if (nullChecker(mainData)) {
                                alert('isi data dulu')
                            } else {
                                editMenu()
                            }
                        }}
                    >
                        Edit Menu
                    </Button>
                    <div style={{ height: '10px' }} />
                    <Button
                        variant='contained'
                        size='large'
                        fullWidth
                        disabled={loading}
                        style={{ backgroundColor: 'black', color: 'white' }}
                        onClick={() => {
                            var r = window.confirm("Are you Sure ?");
                            if (r == true) {
                                deleteMenu()
                            } else {
                                console.log('cencel')
                            }
                        }}
                    >
                        Hapus Menu
                    </Button>
                </Grid>
            </Grid>
        </div>
    );
}

export default withRouter(EditMenu);