import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import { Typography, Grid, TextField, Button, Dialog, DialogTitle, DialogContent } from '@material-ui/core';
import { Image, AddBox } from '@material-ui/icons';
import { EditorState, convertToRaw, convertFromRaw, ContentState, convertFromHTML } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import { stateFromHTML } from 'draft-js-import-html';
import { stateToHTML } from 'draft-js-export-html';
// import htmlToDraft from 'html-to-draftjs';
// assets
import initialImage from '../assets/initial-image.jpg'
// import androidFrame from '../assets/android-frame.png';
// api
import { Api, imageUrl } from '../components/Api';
import { nullChecker } from '../components/PayloadChecker';
import Previewer from '../components/previewer';

// const initialMarkup =
//     'Tuangkan <b>Materi anda</b>, <i>Disini</i><br/ ><br />'

const Database = { "idMenu": 5, "title": "Tips Mencehah DBD", "imageMenu": "file-1569738098670.jpg", "contentMenu": "{\"__html\":\"<p><strong>Cara pencegahan demam berdarah (DBD) di rumah</strong></p>\\n<p></p>\\n<iframe width=\\\"auto\\\" height=\\\"auto\\\" src=\\\"https://www.youtube.com/embed/LByY65tCdPs\\\" frameBorder=\\\"0\\\"></iframe>\\n<p></p>\\n<p><a href=\\\"https://hellosehat.com/penyakit/demam-berdarah-dengue-dbd/\\\" target=\\\"_self\\\">Demam berdarah dengue</a> (DBD) adalah <a href=\\\"https://hellosehat.com/hidup-sehat/fakta-unik/penyakit-menular-lewat-gigitan-nyamuk/\\\" target=\\\"_self\\\">penyakit menular yang disebabkan oleh </a><a href=\\\"https://hellosehat.com/hidup-sehat/fakta-unik/penyakit-menular-lewat-gigitan-nyamuk/\\\" target=\\\"_self\\\">nyamuk </a>Aedes aegypti pembawa virus dengue. DBD yang terlambat dikenali dan diobati dapat mengakibatkan perdarahan dalam berbahaya. Maka, butuh upaya pencegahan demam berdarah (DBD) dari diri sendiri dan orang di sekitar rumah agar penyakit ini tidak semakin menyebar luas. Bagaimana caranya?</p>\\n<p>Mungkin Anda sendiri sudah sangat familier dengan slogan pencegahan demam berdarah (DBD) yang berbunyi “3M: menguras, menutup, dan mengubur”. Namun, prinsip pencegahan DBD bukan cuma itu.</p>\\n<p>Cara yang paling utama adalah dengan mengusahakan agar Anda <a href=\\\"https://hellosehat.com/hidup-sehat/tips-sehat/supaya-tidak-digigit-nyamuk-bahan/\\\" target=\\\"_self\\\">tidak digigit nyamuk</a> <em>Aedes aegypti</em>. Ini bisa dilakukan dengan menjaga lingkungan tetap bersih, juga menggunakan penangkal nyamuk agar tidak berkembang biak di rumah.</p>\\n\"}" };
// console.log(JSON.parse(Database.contentMenu).__html);
// const initialMarkup = JSON.parse(Database.contentMenu).__html;
const initialMarkup = '<p>Hey this <strong>editor</strong> rocks 😀</p>';
const state = stateFromHTML(initialMarkup)


function AddMenu(props) {

    const [modal, setModal] = useState({
        open: false,
        flag: true
    });
    const [mainData, setMainData] = useState({
        title: '',
        imageTitle: '',
    })
    const [editorState, setEditorState] = useState(EditorState.createWithContent(state))
    const [imageStore, setImageStore] = useState([])
    const [loading, setLoading] = useState(false)

    function getButtonImage(MainElement) {
        let inputElement = MainElement.getElementsByClassName('rdw-image-modal-url-input')[0];
        if (inputElement !== undefined) {
            inputElement.value = 'joni'
        }
    }

    function editElement() {
        let imageButton = window.document.getElementsByClassName('rdw-image-wrapper')[0];
        imageButton.addEventListener('click', function () {
            // handleClickOpen(false)
            getButtonImage(imageButton)
        })
    }

    useEffect(() => {
        getImage();
        // editElement();
    }, []);

    function addMenu() {
        setLoading(true)

        let payload = {
            title: mainData.title,
            imageMenu: mainData.imageTitle,
            contentMenu: JSON.stringify(createMarkup())
        }

        Api.post('/menu', payload)
            .then(res => {
                setLoading(false)
                if (res.data.status === 200) {
                    alert('data berhasil ditambah')
                    props.history.goBack()
                } else {
                    alert(res)
                }
            })
            .catch(err => {
                alert(err)
            })

    }

    function handleUploadFile(event) {
        const data = new FormData();
        data.append('file', event.target.files[0]);
        Api.post('/fileUploader', data)
            .then(res => {
                getImage()
            })
            .catch(err => {
                alert(err)
            })
    }

    function getImage() {
        Api.get('/list-image')
            .then(res => {
                if (res.data.status === 200) {
                    let data = res.data.payload;
                    setImageStore(res.data.payload);
                } else {
                    alert('error')
                }
            })
            .catch(err => {
                alert(err)
            })
    }

    function upload(file) {
        const data = new FormData();
        data.append('file', file);
        return new Promise(
            (resolve, reject) => {
                Api.post('/fileUploader', data)
                    .then(res => {
                        resolve({
                            data: {
                                link: imageUrl + res.data.payload.filename
                            }
                        });
                    })
                    .catch(err => {
                        alert(err)
                    })
            }
        );
    }

    function chooseImage(data) {
        setModal({ ...modal, ['open']: false });
        setMainData({ ...mainData, ['imageTitle']: data.image });
    }

    function ModalImageStore() {
        return (
            <Dialog onClose={handleClose} open={modal.open}>
                <DialogTitle onClose={handleClose} className='secondary-bg'>
                    <Typography
                        style={{
                            color: '#FFF',
                            fontSize: '25px'
                        }}>
                        Choose Image From Database
                    </Typography>
                </DialogTitle>
                <DialogContent dividers>
                    <div>
                        <Grid container spacing={3}>
                            {imageStore.map((data, i) => (
                                <Grid item xs={4} key={i.toString()}>
                                    <Button
                                        onClick={() => { chooseImage(data) }}
                                    >
                                        <img
                                            src={imageUrl + data.image}
                                            alt={data.original_name}
                                            className='image-store-preview'
                                        />
                                    </Button>
                                    <p className='title-menu'>
                                        {data.image}
                                    </p>
                                </Grid>
                            ))}
                            <Grid item xs={4} className='flex-center-column' style={{
                                justifyContent: 'space-around'
                            }}>
                                <input
                                    accept="image/*"
                                    style={{
                                        display: 'none'
                                    }}
                                    id={'upload-id'}
                                    type="file"
                                    onChange={handleUploadFile}
                                />
                                <label htmlFor={'upload-id'}>
                                    <Button
                                        style={{ padding: 0 }}
                                        component="span"
                                    >
                                        <AddBox color={'secondary'} style={{
                                            fontSize: '130px',
                                            textAlign: 'center'
                                        }} />
                                    </Button>
                                </label>
                                <p className='title-menu'>
                                    Add Image
                                </p>
                            </Grid>
                        </Grid>
                    </div>
                </DialogContent>
            </Dialog>
        )
    }

    const handleClickOpen = (flag) => {
        if (flag === false) {
            console.log('sip')
        }
        setModal({ ...modal, ['open']: true });
    };
    const handleClose = () => {
        setModal({ ...modal, ['open']: false });
    };

    const handleChange = name => event => {
        setMainData({ ...mainData, [name]: event.target.value });
    };

    function onEditorStateChange(editorState) {
        console.log(createMarkup())
        setEditorState(editorState)
    };

    function createMarkup() {
        return { __html: stateToHTML(editorState.getCurrentContent()) };
        // return { __html: draftToHtml(convertToRaw(editorState.getCurrentContent())) };
    }

    return (
        <div>
            <ModalImageStore />
            <Grid container spacing={3}>
                <Grid item xs={12} md={7}>
                    <Grid item style={{ padding: '10px' }}>
                        <div className={'container-editor-title'}>
                            <div className='flex-center-column primary-bg' style={{
                                borderRadius: '15px 15px 0 0'
                            }}>
                                <h1 style={{ margin: '5px' }}>Menu Editor</h1>
                            </div>
                            <div style={{
                                padding: '10px'
                            }}>
                                <Grid container>
                                    <Grid item xs={8} className='flex-center-column'>
                                        <div style={{ width: '350px' }}>
                                            <TextField
                                                label="Title"
                                                value={mainData.title}
                                                onChange={handleChange('title')}
                                                margin="normal"
                                                placeholder={'Input Title'}
                                                variant="outlined"
                                                fullWidth
                                                inputProps={{
                                                    maxLength: 20,
                                                }}
                                            />
                                            <div style={{ height: '30px' }} />
                                            <Button
                                                variant="contained"
                                                component="span"
                                                size={'large'}
                                                color={'secondary'}
                                                onClick={() => {
                                                    handleClickOpen()
                                                }}
                                            >
                                                <Image /> &nbsp; Upload Image
                                            </Button>
                                        </div>
                                    </Grid>
                                    <Grid item xs={4} style={{ padding: '10px' }}>
                                        <div style={{
                                            borderRadius: '17px',
                                            border: '2px solid #2196F3'
                                        }}>
                                            <div className='flex-center-column primary-bg' style={{
                                                borderRadius: '15px 15px 0 0'
                                            }}>
                                                <h2 style={{ margin: '5px' }}>
                                                    Menu Preview
                                                </h2>
                                            </div>
                                            <div className='flex-center-column' style={{ minHeight: '200px' }}>
                                                <div className='menu-preview'>
                                                    <div className='flex-center-column'
                                                        style={{
                                                            height: 'calc(100% - 30px)'
                                                        }}>
                                                        <img
                                                            src={mainData.imageTitle.length === 0 ? initialImage : imageUrl + mainData.imageTitle}
                                                            alt='image'
                                                            className='image-menu'
                                                        />
                                                    </div>
                                                    <div>
                                                        <p className='title-menu'>{mainData.title}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </Grid>
                                </Grid>
                            </div>
                        </div>
                    </Grid>
                    <Grid item>
                        <div className={'container-editor'}>
                            <Editor
                                editorState={editorState}
                                wrapperClassName="wrapper"
                                editorClassName="editor"
                                onEditorStateChange={onEditorStateChange}
                                toolbar={{
                                    image: {
                                        // uploadCallback: upload,
                                        previewImage: true,
                                        uploadEnabled: true
                                    }
                                }}
                            />
                        </div>
                    </Grid>
                </Grid>
                <Grid item xs={12} md={5}>
                    <div className='container-editor-title' style={{
                        marginTop: '10px'
                    }}>
                        <div className='flex-center-column primary-bg'
                            style={{
                                borderRadius: '15px 15px 0 0'
                            }}>
                            <h1 style={{ margin: '5px' }}>Content Preview</h1>
                        </div>
                        <div className='flex-center-column'
                            style={{
                                padding: '50px 0'
                            }}>
                            <Previewer markup={createMarkup()} mainData={mainData} />
                        </div>
                    </div>
                    <div className='space' />
                    <Button
                        variant='contained'
                        size='large'
                        fullWidth
                        color='secondary'
                        disabled={loading}
                        onClick={() => {
                            if (nullChecker(mainData)) {
                                alert('isi data dulu')
                            } else {
                                addMenu()
                            }
                        }}
                    >
                        Save
                    </Button>
                </Grid>
            </Grid>
        </div>
    );
}

export default withRouter(AddMenu);