import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { EditorState, convertToRaw, convertFromRaw, ContentState, convertFromHTML } from 'draft-js';
import {
    BrowserView,
    MobileView
} from "react-device-detect";
import draftToHtml from 'draftjs-to-html';
// assets
// api
import { Api, imageUrl } from '../components/Api';

function DetailMenu(props) {

    const [mainData, setMainData] = useState({
        title: '',
        imageTitle: '',
    })
    const [editorState, setEditorState] = useState(EditorState.createEmpty())

    useEffect(() => {
        let payload = props.location.pathname.split('/');
        if (payload[2] === undefined) {
            props.history.goBack()
        } else {
            getDetail(payload[2]);
        }
    }, []);

    function overideDoom() {
        let element = window.document.getElementById('inner-html').getElementsByTagName('img');
        function doRide() {
            setTimeout(
                function () {
                    for (let data of element) {
                        data.classList.add('image-content');
                    }
                }, 1000);
        }
        console.log(element)
        doRide()
    }

    function initialMarkup(payload) {
        let innerHtml = JSON.parse(payload).__html;
        let blocksFromHTML = convertFromHTML(innerHtml);
        let state = ContentState.createFromBlockArray(
            blocksFromHTML.contentBlocks,
            blocksFromHTML.entityMap
        );
        setEditorState(EditorState.createWithContent(state))
    }

    function getPayload(data) {
        setMainData({
            ...mainData,
            ['title']: data.title,
            ['imageTitle']: data.image_menu
        });
        initialMarkup(data.content_menu);
        overideDoom();
    }

    function getDetail(ID) {
        Api.get('/detail-menu?idMenu=' + ID)
            .then(res => {
                if (res.data.status === 200) {
                    getPayload(res.data.payload[0])
                } else {
                    alert(res)
                }
            })
            .catch(err => {
                alert(err)
            })
    }

    function createMarkup() {
        return { __html: draftToHtml(convertToRaw(editorState.getCurrentContent())) };
    }

    return (
        <div>
            <BrowserView>
                <div className='flex-center-column'
                    style={{
                        padding: '50px 0'
                    }}>
                    <div className='android-frame'>
                        <div className='device-container'>
                            <img
                                src={imageUrl + mainData.imageTitle}
                                alt={mainData.imageTitle}
                                className='image-title'
                            />
                            <div
                                style={{
                                    padding: '10px'
                                }}
                                id='inner-html'
                                dangerouslySetInnerHTML={createMarkup()}
                            />
                        </div>
                    </div>
                </div>
            </BrowserView>
            <MobileView>
                <img
                    src={imageUrl + mainData.imageTitle}
                    alt={mainData.imageTitle}
                    className='image-title'
                />
                <div
                    style={{
                        padding: '10px'
                    }}
                    id='inner-html'
                    dangerouslySetInnerHTML={createMarkup()}
                />
            </MobileView>
        </div>
    );
}

export default withRouter(DetailMenu);