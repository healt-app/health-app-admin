import React from 'react';
import { Switch, Route } from 'react-router-dom';
import './App.css';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import Main from './pages';
import DetailMenu from './pages/DetailMenu';
import Welcome from './components';
import EditMenu from './pages/EditMenu';
import AddMenu from './pages/AddMenu';
// import LinearProgress from '@material-ui/core/LinearProgress';

function App(props) {
  return (
    <div>
      {/* <LinearProgress color={'secondary'} /> */}
      <div className='container'>
        <Switch>
          <Route exact path="/" component={Welcome} />
          <Route path="/menu" component={Main} />
          <Route path="/detail-menu" component={DetailMenu} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
